<?php

namespace App\Services\Contracts;


use App\Entities\Photo;

interface PhotoService
{
    public function crop(string $pathToPhoto, int $width, int $height): string;

    public function save(
        string $fileName,
        int $userId,
        string $status,
        string $photo100_100 = null,
        string $photo150_150 = null,
        string $photo250_250 = null
    ): void;

    public function findByUserId(int $userId): Photo;
}
