<?php

namespace App\Services;

use App\Entities\Photo;
use App\Services\Contracts\PhotoService as IPhotoService;
use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use App\Services\Contracts\AuthService;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class PhotoService implements IPhotoService
{
    private $authService;
    private $fileSystem;

    public function __construct(Filesystem $fileSystem, AuthService $authService)
    {
        $this->fileSystem = $fileSystem;
        $this->authService = $authService;
    }

    public function crop(string $pathToPhoto, int $width, int $height): string
    {
        $croppedImage = Image::make(
            Storage::path($pathToPhoto)
        )->crop($width, $height);
        $filePath = 'cropped/'
            . Str::random(40)
            . '.'
            . $croppedImage->extension;

        $this->fileSystem->put(
            $filePath,
            $croppedImage->stream()
        );

        return $filePath;
    }

    public function save(
        string $fileName,
        int $userId,
        string $status,
        string $photo100_100 = null,
        string $photo150_150 = null,
        string $photo250_250 = null
    ): void
    {
        Photo::create([
            'user_id' => $userId,
            'original_photo' => $fileName,
            'status' => $status,
            'photo_100_100' => $photo100_100,
            'photo_150_150' => $photo150_150,
            'photo_250_250' => $photo250_250
        ]);
    }

    public function findByUserId(int $userId): Photo
    {
        return Photo::where('user_id', $userId)->orderBy('id', 'desc')->first();
    }
}
