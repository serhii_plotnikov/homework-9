<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Entities\User;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\Contracts\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $photo;
    public $tries = 1;
    private $user;
    private $extensions = ['jpeg', 'jpg', 'bmp', 'png'];
    private $params = [['width' => 100, 'height' => 100], ['width' => 150, 'height' => 150],
        ['width' => 250, 'height' => 250]];

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = User::find($this->photo->user_id);
    }


    public function handle(PhotoService $photoService)
    {
        [$name, $extension] = explode('.', $this->photo->original_photo);
        if (!in_array($extension, $this->extensions)) {
            throw new \Exception("{$extension}  - wrong image permission");
        }

        $generatedImages = [];
        foreach ($this->params as $param) {
            $filePath = $photoService->crop($this->photo->original_photo, $param['width'], $param['height']);
            $newFilePath = "{$name}{$param['width']}x{$param['height']}.{$extension}";
            $generatedImages[] = $newFilePath;
            Storage::move($filePath, $newFilePath);
        }
        [$photo100_100, $photo150_150, $photo250_250] = $generatedImages;
        $this->photo->fill([
            "photo_100_100" => $photo100_100,
            "photo_150_150" => $photo150_150,
            "photo_250_250" => $photo250_250,
            'status' => 'SUCCESS'
        ]);
        $this->photo->save();

        $this->user->notify(new ImageProcessedNotification($this->photo));
    }

    public function failed(\Exception $exception)
    {
        $this->photo->fill(['status' => 'FAIL']);
        $this->photo->save();
        $this->user->notify(new ImageProcessingFailedNotification($this->photo));
    }
}
