<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use App\Jobs\CropJob;
use App\Services\Contracts\PhotoService;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    private $photoService;

    public function __construct(PhotoService $photoService)
    {
        $this->photoService = $photoService;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('photo');
        $fileName = $file->storeAs("images/{$request->user()->id}", $file->getClientOriginalName());
        $this->photoService->save($fileName, $request->user()->id, "UPLOADED");
        $photo = $this->photoService->findByUserId($request->user()->id);
        CropJob::dispatch($photo);
    }


}
