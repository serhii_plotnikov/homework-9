<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    public $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Dear {$this->photo->user->name},")
            ->line('Photos have been successfully uploaded and processed.')
            ->line('THere are links to the images:')
            ->line($this->photo->photo_100_100)
            ->line($this->photo->photo_150_150)
            ->line($this->photo->photo_250_250)
            ->line("Thanks!");
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'status' => strtolower($this->photo->status),
                'photo_100_100' => $this->photo->photo_100_100,
                'photo_150_150' => $this->photo->photo_150_150,
                'photo_250_250' => $this->photo->photo_250_250
            ]
        );
    }

}
